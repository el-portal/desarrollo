// Indexes

db.getSiblingDB('world_entries').entries.createIndex(
    { "$**":"text" },
    { 
        name: "searcherIndex",
        weights: { name: 20, publicDescription: 10, longDescription: 5 }, 
        default_language: "spanish" 
    }
)

db.getSiblingDB('world_entries').entries.createIndex(
    { entryClass: 1, created_ts: -1 },
    { name: "entryClassBoardIndex" }
)

db.getSiblingDB('world_entries').entries.createIndex(
    { news_ts:-1, _id:1 },
    { 
        name: "newsBoardIndex",
        partialFilterExpression: { entryClass: "events", diffusion: {$exists: true} }
    }
)

db.getSiblingDB('world_entries').entries.createIndex(
    { entryClass: 1, name: 1 },
    { name: "entryClassOptionsIndex" }
)

db.getSiblingDB('world_entries').links.createIndex(
    { _id1: 1, field1: 1, _id2: 1 },
    { name: "linksByIdIndex1" }
)

db.getSiblingDB('world_entries').links.createIndex(
    { _id2: 1, field2: 1, _id1: 1 },
    { name: "linksByIdIndex2" }
)

db.getSiblingDB('world_entries').links.createIndex(
    { _id2:1, created_ts: -1 },
    { 
        name: "linksForHistoryIndex",
        partialFilterExpression: {field2: "involvedIn"}
    }
)

db.getSiblingDB('world_entries').options.createIndex(
    { field: 1, name: 1 },
    { name: "fieldOptionsIndex" }
)

// Secondary databases

db.getSiblingDB('world_entries').options.insertMany([
    {name:'Reino', field:"locationType"},
    {name:'Ciudad', field:"locationType"},
    {name:'Pueblo', field:"locationType"},
    {name:'Punto Clave', field:"locationType"},
])

db.getSiblingDB('world_entries').options.insertMany([
    {name:'Humano/a', field:"race"},
    {name:'Semielfo/a', field:"race"},
    {name:'Elfo/a', field:"race"},
    {name:'Enano/a', field:"race"},
    {name:'Gnomo/a', field:"race"},
    {name:'Mediano/a', field:"race"},
    {name:'Dracónido/a', field:"race"},
    {name:'Tiefling', field:"race"},
    {name:'Semiorco/a', field:"race"},
])

db.getSiblingDB('world_entries').options.insertMany([
    {name:'Aventurero/a', field:"occupation"},
    {name:'Guardia', field:"occupation"},
    {name:'Tabernero/a', field:"occupation"},
    {name:'Granjero/a', field:"occupation"},
])

db.getSiblingDB('world_entries').options.insertMany([
    {name:'Nacionalidad', field:"societyType"},
    {name:'Religión', field:"societyType"},
    {name:'Sociedad Secreta', field:"societyType"},
    {name:'Culto', field:"societyType"},
    {name:'Parlamento', field:"societyType"},
    {name: 'Grupo de aventurero/as', field:"societyType"},
    {name: 'Grupo de villano/as', field:"societyType"},
])

db.getSiblingDB('world_entries').options.insertMany([
    {name:'Espada', field:"objectType"},
    {name:'Anillo', field:"objectType"},
    {name:'Arco', field:"objectType"},
    {name:'Poción', field:"objectType"},
])

// Primary databases

db.getSiblingDB('world_entries').entries.insertMany([
    {
        entryClass:'locations',
        x: 38,
        y: 68,
        name:'La Comarca',
        publicDescription:'Región de colinas verdes y campos vivarachos en las que viven los medianos dentro de casas de techos bajos y puertas y ventanas redondas.',
        locationType: 'Región',
        fame: 'Baja',
        danger: 'Bajo',
        longDescription: 'Tierras verdes y pacíficas, que incluyen colinas, llanuras, campos y bosques de los que los medianos están enamorados. Se divide políticamente en cuatro cuadernas, siendo la Cuaderna del Oeste la más habitada de todas. Es allí donde se sitúa Hobbiton.\nAunque los medianos no lo saben y viven con poco interés en el exterior, los hombres Montaraces protegen estas tierras de posibles ataques enemigos.',
        created_ts: Date.now() - Math.floor(Math.random() * 1000),
    },  
    {
        entryClass:'locations',
        x: 40,
        y: 33,
        name: 'Gondor',
        publicDescription: 'Famoso y antiguo reino de los humanos, regentado por senescales y aliado al reino de Rohan.',
        locationType: 'Reino',
        fame: 'Alta',
        danger: 'Bajo',
        longDescription: 'Gondor es el nombre que recibió el reino del exilio del Sur que fue fundado después de la Caída de Númenor por Isildur y su hermano Anárion.\n\nA pesar de que el linaje de sus reyes se perdió después de 31 reyes y fuera posteriormente gobernado por los senescales, Gondor psobrevivió y tuvo un papel clave en la Guerra del Anillo, por encontrarse a la cabeza del bando que luchaba contra Sauron.',
        created_ts: Date.now() - Math.floor(Math.random() * 1000),
    },  
    {
        entryClass:'locations',
        x: 52,
        y: 20,
        name: 'Minas Tirith',
        publicDescription: 'Capital del reino de Gondor: gran ciudad escalonada de bella piedra blanca organizada en anillos concéntricos sobre la ladera de una colina.',
        locationType: 'Ciudad',
        fame: 'Alta',
        danger: 'Bajo',
        longDescription: 'Minas Tirith es la capital del reino humano de Gondor. Es una ciudad poblada por casi diez mil habitantes, situada en los Campos del Pelennor y construida sobre un risco en La Colina de la Guardia sobre la ladera oriental del monte Mindolluin. La ciudad está dividida en siete niveles rodeados de murallas blancas de piedra. En el séptimo nivel se encuentra la Ciudadela con la Torre Blanca de Ecthelion y el simbólico Árbol Blanco situado en la Plaza del Manantial.',
        created_ts: Date.now() - Math.floor(Math.random() * 1000),
    },  
    {
        entryClass:'locations',
        x: 33,
        y: 52,
        name: 'Moria',
        publicDescription: 'El más antiguo y famoso reino de los enanos situado en las minas más grandes jamás construídas en el corazón de las Montañas Nubladas.',
        locationType: 'Reino',
        fame: 'Media',
        danger: 'Alto',
        longDescription: 'Moria ("pozo oscuro"), también conocida como Khazad-dûm (mansión de los enanos), fue durante mucho tiempo el símbolo del poderío del pueblo enano: un gran centro de comercio y minería en sus interminables y bellas galerías subterráneas. Sin embargo, se rumorea que la prosperidad terminó cuando los enanos excavaron demasiado hondo en busca de mithril. Aunque pocos lo saben, fue entonces que despertaron a un antiguo Balrog y las minas se convirtieron en un lugar de oscuridad y miedo.',
        created_ts: Date.now() - Math.floor(Math.random() * 1000),
    },  
    {
        entryClass:'locations',
        x: 54,
        y: 4,
        name: 'Mordor',
        publicDescription: 'Región desolada y rodeada de montañas donde Sauron, el Señor Oscuro, edificó su fortaleza Barad-dûr para atacar y dominar la Tierra Media.',
        locationType: 'Región',
        fame: 'Alta',
        danger: 'Alto',
        longDescription: 'Región desolada y plana, con un interior desértico situado a gran altitud y rodeado de una muralla natural formada por Ered Lithui, las Montañas de Ceniza, y Ephel Duáth, las Montañas de la Sombra. En su interior, el Señor Oscuro Sauron edificó su fortaleza Barad-dûr, la Torre Oscura, y desde allí vigila la Tierra Media, a la que aún quiere dominar. Dentro de Mordor se halla el gigantesco volcán llamado el Monte del Destino donde el Anillo Único fue forjado. Sólo allí puede ser destruido.',
        created_ts: Date.now() - Math.floor(Math.random() * 1000),
    },  
])

db.getSiblingDB('world_entries').entries.insertMany([
    {
        entryClass:'individuals',
        name:'Frodo Bolsón',
        publicDescription:'Mediano amable y callado de ojos azules y piel clara, sobrino de Bilbo Bolsón y portador del Anillo Único y del destino de la Tierra Media.',
        race:'Mediano/a',
        occupation:'Aventurero/a',
        influence:'Alta',
        danger:'Bajo',
        longDescription:'Frodo es un hobbit de ojos azules claros y que tiene una piel blanca y pálida. Es algo diferente a los demás: no come demasiado, le gusta leer y es más sensible que los demás, siempre abierto a escuchar noticias de afuera de la Comarca a través del amigo de su tío, Gandalf el Gris. Actualmente es el portador del Anillo Único, legado por Bilbo Bolsón. Forma parte de la Comunidad del Anillo y debe llevarlo en persona hasta el terrible Monte del Destino y destruirlo en los fuegos que lo forjaron.',
        created_ts: Date.now() - Math.floor(Math.random() * 1000),
    },
    {
        entryClass:'individuals',
        name:'Sam Samsagaz Gamyi',
        publicDescription:'Hobbit castaño con un gran corazón y una resistencia increíble, siempre leal a Frodo Bolsón en la misión para destruir el anillo.',
        race:'Mediano/a',
        occupation:'Aventurero/a',
        influence:'Media',
        danger:'Bajo',
        longDescription:'Samsagaz Gamyi es un hobbit de cabello castaño y una resistencia increíble que solía ser el jardinero de Bolsón de Tirada, la adorada propiedad de Bilbo Bolsón, tío de Frodo Bolsón. Es humilde y está embelesado con Rosita Coto, una mediana de la Comarca. Fue elegido por Gandalf para acompañar a Frodo en su viaje y luego fue escogido parte de la Comunidad del Anillo porque así lo quiso, siempre leal a Frodo Bolsón a quien seguirá hasta el fin del mundo en su viaje para destruir el Anillo.',
        created_ts: Date.now() - Math.floor(Math.random() * 1000),
    },
    {
        entryClass:'individuals',
        name:'Pippin Peregrin Tuk',
        publicDescription:'Hobbit de cabello dorado y enrulado, curioso, travieso y gracioso, que es amigo de Frodo y Merry, integrante de la Comunidad del Anillo.',
        race:'Mediano/a',
        occupation:'Aventurero/a',
        influence:'Media',
        danger:'Bajo',
        longDescription:'Pippin es un hobbit de cabello enrulado, casi dorado, alegre y con un gran sentido del humor, lleno de preguntas curiosas e inocentes. Es primo y continuo cómplice de Merry Brandigamo, amigo de Frodo Bolsón. Forma parte de la Comunidad del Anillo y según Gandalf tiene un talento especial para hacer la cosa equivocada en el momento equivocado. Puede parecer inconciente, pero es capaz de ser muy valeroso y volverse maduro.',
        created_ts: Date.now() - Math.floor(Math.random() * 1000),
    },
    {
        entryClass:'individuals',
        name:'Merry Meriadoc Brandigamo',
        publicDescription:'Mediano muy inteligente y perceptivo, primo y amigo de Frodo Bolsón, muy cercano a Pippin Tuk, todos miembros de la Comunidad del Anillo.',
        race:'Mediano/a',
        occupation:'Aventurero/a',
        influence:'Media',
        danger:'Bajo',
        longDescription:'Merry es un mediano muy inteligente, sabiohondo y altamente perceptivo, siempre organizado y preparado. Es el primo de Frodo Bolsón que conspiró con Pippin Tuk y con Sam Gamyi para que éste no dejara solo la Comarca en su misión de trasladar el anillo. Es muy amigo de Pippin, con quien solían hacer estragos en la Comarca. Actualmente forma parte de la Comunidad del Anillo y acompaña a los demás hacia Mordor y el Monte del Destino donde debe ser destruido el Anillo Único.',
        created_ts: Date.now() - Math.floor(Math.random() * 1000),
    },
    {
        entryClass:'individuals',
        name:'Aragorn Trancos Elessar Telcontar',
        publicDescription:'Hombre humilde, serio y sabio que se presenta como el trotamundos "Trancos" pero es en realidad el heredero de Isildur y del trono de Gondor.',
        race:'Mediano/a',
        occupation:'Aventurero/a',
        influence:'Media',
        danger:'Bajo',
        longDescription:'Hombre del oeste de aspecto desaliñado, serio pero de buen corazón, que es miembro de la Comunidad del Anillo y que actualmente viaja hacia Mordor y su destino. Conocido como el trotamundos "Trancos", estuvo por muchos años al cuidado de Elrond en Rivendel, ocultando su verdadera identidad. Está enamorado de Arwen, hija de Elrond, con la que sólo le será permitido casarse si se convierte en el rey de Gondor de cuyo trono es sucesor por ser hijo de Arathorn II y Gilraen, heredero de Isildur.',
        created_ts: Date.now() - Math.floor(Math.random() * 1000),
    },
])

db.getSiblingDB('world_entries').entries.insertMany([
    {
        entryClass:'societies',
        name:'Comunidad del Anillo',
        publicDescription:'Grupo de nueve aventureros de razas variadas cuya misión es escoltar a Frodo Bolsón y el Anillo Único al Monte del Destino para destruirlo.',
        societyType:'Grupo de aventurero/as',
        danger:'Medio',
        influence:'Alta',
        members:'Baja',
        secretism:'Alto',
        longDescription:'También conocida como la Compañía del Anillo, esta asociación fue creada en Rivendel durante el Concilio de Elrond. Buscando una solución al inminente retorno de Sauron, representantes de varias razas y naciones acudieron al llamado. Debido a antiguas enemistades surgieron disputas que sólo se solucionaron cuando Frodo propuso que él llevaría el anillo al Monte del Destino en Mordor para destruirlo. Movidos por su valentía, otros ocho aventureros se unieron a pesar de sus diferencias.',
        created_ts: Date.now() - Math.floor(Math.random() * 1000),
    },
    {
        entryClass:'societies',
        name:'Nazgûl',
        publicDescription:'Grupo de nueve espectros comandados por Sauron que persiguen a Frodo y la Comunidad del Anillo en respuesta al llamado del Anillo Único.',
        societyType:'Grupo de villano/as',
        danger:'Alto',
        influence:'Alta',
        members:'Baja',
        secretism:'Medio',
        longDescription:'Los Nazgûl eran nueve hombres, grandes magos y guerreros. El señor oscuro Sauron les ofreció un anillo de poder a cada uno que les llevó a su perdición: sufrían cambios de aspecto, por momentos se volvían invisibles y una maldad crecía cada vez más y más dentro de ellos.\nUno de ellos, el Rey Brujo, llegó antaño a comandar un ejército bajo órdenes de la Corona de Hierro. La amenaza fue vencida, pero el Rey Brujo y ocho seguidores escaparon, ahora espectros a las órdenes de Sauron.',
        created_ts: Date.now() -  Math.floor(Math.random() * 1000),
    },
])

db.getSiblingDB('world_entries').entries.insertMany([
    {
        entryClass:'objects',
        name:'El Anillo Único',
        publicDescription:'Poderoso anillo mágico de peligrosa influencia forjado por el Señor Oscuro Sauron para controlar al resto y esclavizar la Tierra Media.',
        influence:'Alta',
        danger:'Alto',
        objectType:'Anillo',
        magic:true,
        magicRarity:'Legendario',
        longDescription:'Anillo de oro que se ajustaba al dedo del portador, con un grabado en la Lengua Negra, el cual decía: "Un Anillo para gobernarlos a todos, un Anillo para encontrarlos, un Anillo para atraerlos a todos y en las tinieblas atarlos".\n\nFue creado por Sauron en el Monte del Destino después de engañar a los herreros elfos de Eregion para que le enseñaran el arte de forjar anillos. Su intención era dominar con él a todos los otros Anillos de Poder y esclavizar así a los Pueblos Libres de la Tierra Media.',
        created_ts: Date.now() -  Math.floor(Math.random() * 1000),
    },
    {
        entryClass:'objects',
        name:'Andúril, la Flama del Oeste',
        publicDescription:'Espada larga y noble forjada de los restos de Narsil, el arma con la cual Isildur cortó el dedo donde Sauron portaba el Anillo Único.',
        influence:'Media',
        danger:'Medio',
        objectType:'Espada',
        magic:true,
        magicRarity:'Raro',
        longDescription:'Este arma fue forjada a partir de los restos de Narsil, la espada con la cual Isildur cortó el dedo donde Sauron portaba el Anillo Único. Sus fragmentos sobrevivieron a la Batalla de los Campos Gladios y fueron llevados a Rivendel, custodiados allí por Elrond hasta que Aragorn los reclamó para abrazar su identidad. En la hoja lleva una inscripción en Quenya que reza "Sol. Soy Andúril, que fue Narsil, la espada de Elendil. Que los esbirros de Mordor huyan de mí. Luna".',
        created_ts: Date.now() - Math.floor(Math.random() * 1000),
    },
])

const date = Date.now() + 1120 + Math.floor(Math.random() * 10)

db.getSiblingDB('world_entries').entries.insertMany([
    {
        entryClass:'events',
        name:'Frodo obtiene el anillo',
        publicDescription:'Habiéndose marchado Bilbo, Frodo se encuentra en Bolsón Cerrado con Gandalf y con una carta que contiene un anillo muy singular.',
        impact:'Alto',
        keyInformation:'Frodo obtiene el Anillo Único',
        longDescription:'En una extraña despedida, Bilbo se marcha de repente desapareciendo de su cumpleaños. Frodo vuelve a Bolsón Cerrado y allí se encuentra con un Gandalf de aspecto preocupado. Le dice que Bilbo le ha dejado una carta y en ella encuentra un anillo muy singular. Instado por el mago, lo arroja al fuego y se revela una tétrica inscripción. Afectado, Gandalf decide ir a buscar más información, descubre a Sam espiando la conversación y les encarga a ambos irse velozmente de La Comarca.',
        changes:'Frodo ahora posee el Anillo Único',
        diffusion: [
            {
                diffusionType: "Rumor",
                verisimilitude: "Media",
                content: "Se rumorea que últimamente se ha visto muy nervioso a Sam Gamyi cuando entra y sale de Bolsón Cerrado. ¿Será por lo mismo que tiene a Frodo Bolsón más pálido que de costumbre?",
                created_ts: Date.now() + 1000 + Math.floor(Math.random() * 100)
            }
        ],
        testimonies: [
            { 
                content: '¡Es cierto! El otro día quise jugarle una broma tonta y sobresaltarlo mientras se encargaba del jardín, pero al instante levantó la pala de forma agresiva, con los ojos desorbitados, y gritó: "¡Con el señor Frodo no!"',
                witnessIndividual: [ {_id: db.getSiblingDB('world_entries').entries.find({"name":"Pippin Peregrin Tuk"}).toArray()[0]._id} ],
                isIndividual: true,
                verisimilitude: "Alta",
                created_ts:  Date.now() + 1100 + Math.floor(Math.random() * 10)
            }, // Maybe 2 testimonies each?
            { 
                content: '¡No me digas! ¿Y qué creés que podrá haberles pasado? Porque yo ya cazé dos veces a Frodo mirando con gesto ausente hacia los caminos que salen de La Comarca y con el libro apretado dentro de una mano. Me parece que hasta lo vi tragar saliva.',
                witnessIndividual: [ {_id: db.getSiblingDB('world_entries').entries.find({"name":"Merry Meriadoc Brandigamo"}).toArray()[0]._id} ],
                isIndividual: true,
                verisimilitude: "Alta",
                created_ts:  Date.now() + 1110 + Math.floor(Math.random() * 10)
            },
            { 
                content: 'La verdad que ni idea. Pero supongo que vale la pena investigar, Merry. ¿Qué tal si lo agarramos a Sam entre los dos? Es mucho más fácil de intimidar que Frodo. Je, de hecho no hubo vez que no hayamos logrado sacarle la información que queríamos.',
                witnessIndividual: [ {_id: db.getSiblingDB('world_entries').entries.find({"name":"Pippin Peregrin Tuk"}).toArray()[0]._id} ],
                isIndividual: true,
                verisimilitude: "Alta",
                created_ts: date,
            },
        ],
        created_ts: Date.now() - Math.floor(Math.random() * 1000),
        news_ts: date,
    },
    {
        entryClass:'events',
        name:'Caída de Khazad-dûm',
        publicDescription:'Se rumorea que el reino de Khazad-dûm ha caído, presa de la codicia de los enanos que despertaron algo terrible en lo más profundo de Moria.',
        impact:'Alto',
        keyInformation:'El reino de Khazad-dûm cae ante una invasión de goblins y un balrog.',
        longDescription:'Khazad-dûm había sido durante mucho tiempo el símbolo del poderío del pueblo enano: un gran centro de comercio y minería en sus interminables y bellas galerías subterráneas. Sin embargo, la prosperidad terminó cuando los enanos excavaron demasiado hondo en busca de mithril. Aunque pocos lo saben, fue en este espantoso evento que despertaron a un antiguo Balrog y las minas, infestadas de goblins, se convirtieron en un lugar de oscuridad y terror. Todos los enanos murieron en una feroz batalla.',
        changes:'Khazad-dûm es destruído. Ahora son las minas de Moria, invadidas por goblins y un balrog.',
        created_ts: Date.now() - Math.floor(Math.random() * 1000),
    },
])

db.getSiblingDB('world_entries').links.insertMany([
    {
        field1: 'happenedIn',
        field2: 'involvedIn',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":"Frodo obtiene el anillo"}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"La Comarca"}).toArray()[0]._id,
        entryClass1: "events",
        entryClass2: "locations",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'involvedIndividuals',
        field2: 'involvedIn',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":"Frodo obtiene el anillo"}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"Frodo Bolsón"}).toArray()[0]._id,
        entryClass1: "events",
        entryClass2: "individuals",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'involvedIndividuals',
        field2: 'involvedIn',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":"Frodo obtiene el anillo"}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"Sam Samsagaz Gamyi"}).toArray()[0]._id,
        entryClass1: "events",
        entryClass2: "individuals",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'involvedObjects',
        field2: 'involvedIn',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":"Frodo obtiene el anillo"}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"El Anillo Único"}).toArray()[0]._id,
        entryClass1: "events",
        entryClass2: "objects",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'happenedIn',
        field2: 'involvedIn',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Caída de Khazad-dûm'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"Moria"}).toArray()[0]._id,
        entryClass1: "events",
        entryClass2: "locations",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'relatedIndividuals',
        field2: 'usualLocations',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'La Comarca'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"Frodo Bolsón"}).toArray()[0]._id,
        entryClass1: "locations",
        entryClass2: "individuals",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'relatedIndividuals',
        field2: 'usualLocations',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'La Comarca'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"Sam Samsagaz Gamyi"}).toArray()[0]._id,
        entryClass1: "locations",
        entryClass2: "individuals",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'relatedIndividuals',
        field2: 'usualLocations',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'La Comarca'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"Pippin Peregrin Tuk"}).toArray()[0]._id,
        entryClass1: "locations",
        entryClass2: "individuals",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'relatedIndividuals',
        field2: 'usualLocations',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'La Comarca'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"Merry Meriadoc Brandigamo"}).toArray()[0]._id,
        entryClass1: "locations",
        entryClass2: "individuals",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'relatedIndividuals',
        field2: 'usualLocations',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Gondor'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":'Aragorn Trancos Elessar Telcontar'}).toArray()[0]._id,
        entryClass1: "locations",
        entryClass2: "individuals",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'includesLocations',
        field2: 'includedInLocations',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Gondor'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":'Minas Tirith'}).toArray()[0]._id,
        entryClass1: "locations",
        entryClass2: "locations",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'relatedSocieties',
        field2: 'relatedLocations',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Mordor'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":'Comunidad del Anillo'}).toArray()[0]._id,
        entryClass1: "locations",
        entryClass2: "societies",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'relatedIndividuals',
        field2: 'relatedIndividuals',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Frodo Bolsón'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"Sam Samsagaz Gamyi"}).toArray()[0]._id,
        entryClass1: "individuals",
        entryClass2: "individuals",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'relatedIndividuals',
        field2: 'relatedIndividuals',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Frodo Bolsón'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"Merry Meriadoc Brandigamo"}).toArray()[0]._id,
        entryClass1: "individuals",
        entryClass2: "individuals",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'relatedIndividuals',
        field2: 'relatedIndividuals',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Frodo Bolsón'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"Pippin Peregrin Tuk"}).toArray()[0]._id,
        entryClass1: "individuals",
        entryClass2: "individuals",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'relatedIndividuals',
        field2: 'relatedIndividuals',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Sam Samsagaz Gamyi'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"Pippin Peregrin Tuk"}).toArray()[0]._id,
        entryClass1: "individuals",
        entryClass2: "individuals",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'relatedIndividuals',
        field2: 'relatedIndividuals',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Sam Samsagaz Gamyi'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"Merry Meriadoc Brandigamo"}).toArray()[0]._id,
        entryClass1: "individuals",
        entryClass2: "individuals",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'relatedIndividuals',
        field2: 'relatedIndividuals',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":"Pippin Peregrin Tuk"}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"Merry Meriadoc Brandigamo"}).toArray()[0]._id,
        entryClass1: "individuals",
        entryClass2: "individuals",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'associatedSocieties',
        field2: 'associatedIndividuals',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Frodo Bolsón'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":'Comunidad del Anillo'}).toArray()[0]._id,
        entryClass1: "individuals",
        entryClass2: "societies",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'associatedSocieties',
        field2: 'associatedIndividuals',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Sam Samsagaz Gamyi'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":'Comunidad del Anillo'}).toArray()[0]._id,
        entryClass1: "individuals",
        entryClass2: "societies",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'associatedSocieties',
        field2: 'associatedIndividuals',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Pippin Peregrin Tuk'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":'Comunidad del Anillo'}).toArray()[0]._id,
        entryClass1: "individuals",
        entryClass2: "societies",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'associatedSocieties',
        field2: 'associatedIndividuals',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Merry Meriadoc Brandigamo'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":'Comunidad del Anillo'}).toArray()[0]._id,
        entryClass1: "individuals",
        entryClass2: "societies",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'associatedSocieties',
        field2: 'associatedIndividuals',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Aragorn Trancos Elessar Telcontar'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":'Comunidad del Anillo'}).toArray()[0]._id,
        entryClass1: "individuals",
        entryClass2: "societies",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'notableInventory',
        field2: 'possibleIndividualOwners',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Frodo Bolsón'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"El Anillo Único"}).toArray()[0]._id,
        entryClass1: "individuals",
        entryClass2: "objects",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'notableInventory',
        field2: 'possibleIndividualOwners',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":"Sam Samsagaz Gamyi"}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"El Anillo Único"}).toArray()[0]._id,
        entryClass1: "individuals",
        entryClass2: "objects",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'notableInventory',
        field2: 'possibleIndividualOwners',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Aragorn Trancos Elessar Telcontar'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"Andúril, la Flama del Oeste"}).toArray()[0]._id,
        entryClass1: "individuals",
        entryClass2: "objects",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'notableTreasure',
        field2: 'possibleSocietyOwners',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Comunidad del Anillo'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":"El Anillo Único"}).toArray()[0]._id,
        entryClass1: "societies",
        entryClass2: "objects",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
    {
        field1: 'relatedSocieties',
        field2: 'relatedSocieties',
        _id1: db.getSiblingDB('world_entries').entries.find({"name":'Comunidad del Anillo'}).toArray()[0]._id,
        _id2: db.getSiblingDB('world_entries').entries.find({"name":'Nazgûl'}).toArray()[0]._id,
        entryClass1: "societies",
        entryClass2: "societies",
        created_ts: Date.now() + Math.floor(Math.random() * 1000)
    },
])