# Solo frontend

### Instalaciones previas:
1. [Git](https://git-scm.com/downloads) es un software muy usado para descarga, carga y manejo de repositorios. (Te pregunta muchas cosas, si le dan todo que si esta ok).
2. [Node](https://nodejs.org/es/download) es un runtime environment de javascript que permite hacer desarrollos tanto de backend como de frontend.

### Instalaciones opcionales:
1. [Visual Code](https://code.visualstudio.com/download) es un editor de texto. 

### Entorno de desarrollo:
1. Abri una ventana de comandos de windows: escribi cmd en la lupita y te salta.
2. Navega hasta el directorio donde va a estar el proycto con `cd carpeta` para entrar a las carpetas y `cd ..` para volver atras.
3. Copia y pega lo siguiente para descargar el repositorio de frontend: `git clone https://gitlab.com/el-portal/frontend.git`
4. Entra a la carpeta con `cd frontend`.
5. Escribi `npm install` para instalar dependencias de node en el proyecto.
6. Escribi `npm start` para iniciar el servidor de pruebas.




# Sistema completo

### Requerimientos minimos
- 8 Gb de RAM
- 4 cpus

### Requerimientos recomendados
- 16 Gb de RAM-8 cpus

### Instalaciones previas:
1. [Git](https://git-scm.com/downloads) es un programa muy usado para descarga, carga y manejo de repositorios. 
2. [Docker](https://www.docker.com/products/docker-desktop) es un software para construccion y uso de contenedores (pequeños servers dentro del sistema operativo).
3. Solo si usuas Windows: [WSL](https://www.docker.com/products/docker-desktop) Windows Subsystem for Linux se usa para poder correr Docker en linux:
  . Corre los siguientes comandos: 
  ```
  dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
  dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
  ```
  y luego descarga el [paquete de wsl](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi) y doble click para instalar.
  Por ultimo corre lo siguiente `wsl --set-default-version 2`

### Instalaciones opcionales:
1. [Nodejs](https://nodejs.org/es/download/) es un entorno de ejecucion de javascript. 
2. [Visual Code](https://code.visualstudio.com/download) es un editor de texto. 
3. [Postman](https://www.postman.com/downloads/) es un progrma para hacder request http.
4. [MongoDB Compass](https://www.mongodb.com/try/download/compass) es un programa para visualizar bases de datos no relacionales como MongoDB.
5. [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html) es un programa para administrar conexiones ssh.

### Entorno de desarrollo:
1. Abri una ventana de comandos de windows: escribi cmd en la lupita y te salta.
2. Navega hasta el directorio donde va a estar el proycto con `cd carpeta` para entrar a las carpetas y `cd ..` para volver atras.
3. Copia y pega lo siguiente para descargar el repositorio base: `git clone https://gitlab.com/el-portal/desarrollo.git`
4. Entra a desarrollo con `cd desarrollo`.
5. Escribi `clone.bat` en Windows y `bash clone.bash` en linux para clonar el resto de los repositorios.
6. Escribi `docker-compose up` para crear y levantar los containers.
7. Hace un prueba yendo a [localhost:5003](http://localhost:5003).

